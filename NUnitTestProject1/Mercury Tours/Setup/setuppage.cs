﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using System;
using static NUnitTestProject1.Utilities;

namespace NUnitTestProject1
{
    public class Setuppage
    {
        public static IWebDriver driver;

        [Obsolete]
        public static void Launch_the_application(string browsername)
        {
            if (browsername.Equals("IE"))
                driver = new InternetExplorerDriver(@"C:\IEDriverServer_Win32_3.150.1");

            else if (browsername.Equals("firefox"))
                driver = new FirefoxDriver(@"C:\geckodriver-v0.26.0-win64");

            else
                driver = new ChromeDriver(@"C:\Chromedriver");

            driver.Manage().Window.Maximize();
            driver.Url = "http://newtours.demoaut.com/";
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Name("userName")));
        }

        public static void Enter_username_in_sign_in_page(string userName)
        {
            IWebElement user = driver.FindElement(By.Name("userName"));
            user.SendKeys(userName);
            Assert.AreEqual(user.GetAttribute("value"), userName);
        }
        public static void Enter_password_in_sign_in_page(string value)
        {
            IWebElement pass = driver.FindElement(By.Name("password"));
            pass.SendKeys(value);
            Assert.AreEqual(pass.GetAttribute("value"), value);
        }

        public static void Click_sign_in_button()
        {
            IWebElement sign = driver.FindElement(By.Name("login"));
            sign.Click();
            Implicit_Wait(5);
        }

        public static void Quit_the_application()
        {
            driver.Quit();
        }

    }
}
