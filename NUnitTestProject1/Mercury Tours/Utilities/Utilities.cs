﻿using OpenQA.Selenium;
using System;
using System.Drawing.Imaging;

namespace NUnitTestProject1
{
    class Utilities:Setuppage
    {
        public static void Implicit_Wait(int value)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(value);
        }
        public static void Take_Screenshot()
        {
            ITakesScreenshot screenshotDriver = driver as ITakesScreenshot;
            Screenshot screenshot = screenshotDriver.GetScreenshot();
            String fp = @"C:\Users\deepan.antony\Desktop\Selenium SS\snapshot" + "_" + DateTime.Now.ToString("dd_MMMM_hh_mm_ss_tt") + ".png";
            screenshot.SaveAsFile(fp,ScreenshotImageFormat.Png);
        }

    }
}
