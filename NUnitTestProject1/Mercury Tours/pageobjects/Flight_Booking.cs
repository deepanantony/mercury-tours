﻿using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace NUnitTestProject1
{
    public class Flight_Booking: Setuppage
    {
        public static void Enter_firstname_in_passengers_field(string name,int position)
        {
            IWebElement first = driver.FindElement(By.Name("passFirst"+ position));
            first.SendKeys(name);
            Assert.AreEqual(first.GetAttribute("value"), name);
        }
        public static void Enter_lastname_in_passengers_field(string name, int position)
        {
            IWebElement last = driver.FindElement(By.Name("passLast" + position));
            last.SendKeys(name);
            Assert.AreEqual(last.GetAttribute("value"), name);
        }
        public static void Select_meal_options(string value, int position, string name)
        {
            IWebElement meal = driver.FindElement(By.Name("pass."+position+".meal"));
            SelectElement select = new SelectElement(meal);
            select.SelectByValue(value);
            var meals = select.SelectedOption;
            Assert.AreEqual(meals.Text, name);
        }
        public static void Enter_credit_card_number_in_credit_card_field(string value)
        {
            IWebElement credit = driver.FindElement(By.Name("creditnumber"));
            credit.SendKeys(value);
            Assert.AreEqual(credit.GetAttribute("value"), value);
        }
        public static void Select_expired_month(int value)
        {
            IWebElement month = driver.FindElement(By.Name("cc_exp_dt_mn"));
            SelectElement select = new SelectElement(month);
            select.SelectByIndex(value);
            var months = select.SelectedOption;
            Assert.AreEqual(months.GetAttribute("value"), "06");
        }
        public static void Select_expired_year(string value)
        {
            IWebElement year = driver.FindElement(By.Name("cc_exp_dt_yr"));
            SelectElement select = new SelectElement(year);
            select.SelectByValue(value);
            var years = select.SelectedOption;
            Assert.AreEqual(years.GetAttribute("value"), "2010");
        }
        public static void Enter_credit_card_number_first_name(string value)
        {
            IWebElement first = driver.FindElement(By.Name("cc_frst_name"));
            first.SendKeys(value);
            Assert.AreEqual(first.GetAttribute("value"), value);
        }
        public static void Enter_credit_card_number_middle_name(string value)
        {
            IWebElement middle = driver.FindElement(By.Name("cc_mid_name"));
            middle.SendKeys(value);
            Assert.AreEqual(middle.GetAttribute("value"), value);
        }
        public static void Enter_credit_card_number_last_name(string value)
        {
            IWebElement last = driver.FindElement(By.Name("cc_last_name"));
            last.SendKeys(value);
            Assert.AreEqual(last.GetAttribute("value"), value);
        }
        public static void Click_secure_purchase()
        {
            IWebElement secure_purchase = driver.FindElement(By.Name("buyFlights"));
            secure_purchase.Click();
        }
        public static void Click_logout_button()
        {
            IWebElement logout = driver.FindElement(By.XPath("//img[@src='/images/forms/Logout.gif']"));
            logout.Click();
        }
        public static void Assert_after_logout()
        {
            IWebElement assertlogout = driver.FindElement(By.Name("login"));
            Assert.AreEqual(assertlogout.Displayed, true);
        }
    }
}
