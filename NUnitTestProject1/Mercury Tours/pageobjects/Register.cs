﻿using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using static NUnitTestProject1.Utilities;

namespace NUnitTestProject1
{
    public class Register:Setuppage
    {
        public static void Click_register_for_new_user()
        {
            IWebElement reg = driver.FindElement(By.XPath("//a[text()='REGISTER']"));
            reg.Click();
        }
        public static void Enter_firstname_in_register_page(string name)
        {
            IWebElement first = driver.FindElement(By.Name("firstName"));
            first.SendKeys(name);
            Assert.AreEqual(first.GetAttribute("value"), name);
            Implicit_Wait(2);
        }
        public static void Enter_lastname_in_register_page(string value)
        {
            IWebElement last = driver.FindElement(By.Name("lastName"));
            last.SendKeys(value);
            Assert.AreEqual(last.GetAttribute("value"), value);
            Implicit_Wait(2);
        }
        public static void Enter_phone_number_in_register_page(string value)
        {
            IWebElement phone = driver.FindElement(By.Name("phone"));
            phone.SendKeys(value);
            Assert.AreEqual(phone.GetAttribute("value"), value);
            Implicit_Wait(2);
        }
        public static void Enter_email_id_in_register_page(string value)
        {
            IWebElement mail = driver.FindElement(By.Name("userName"));
            mail.SendKeys(value);
            Assert.AreEqual(mail.GetAttribute("value"), value);
            Implicit_Wait(2);
        }
        public static void Enter_address_in_register_page(string value)
        {
            IWebElement address1 = driver.FindElement(By.Name("address1"));
            address1.SendKeys(value);
            Assert.AreEqual(address1.GetAttribute("value"), value);
            Implicit_Wait(2);
        }
        public static void Enter_city_in_register_page(string value)
        {
            IWebElement city1 = driver.FindElement(By.Name("city"));
            city1.SendKeys(value);
            Assert.AreEqual(city1.GetAttribute("value"), value);
            Implicit_Wait(2);
        }
        public static void Enter_state_in_register_page(string value)
        {
            IWebElement state1 = driver.FindElement(By.Name("state"));
            state1.SendKeys(value);
            Assert.AreEqual(state1.GetAttribute("value"), value);
            Implicit_Wait(2);
        }
        public static void Select_country_in_register_page(string value)
        {
            IWebElement country1 = driver.FindElement(By.Name("country"));
            SelectElement select = new SelectElement(country1);
            select.SelectByValue(value);
            var india = select.SelectedOption;
            Assert.AreEqual(india.GetAttribute("value"), "92");
        }
        public static void Enter_username_in_information_page(string value)
        {
            IWebElement user = driver.FindElement(By.Name("email"));
            user.SendKeys(value);
            Assert.AreEqual(user.GetAttribute("value"), value);
        }
        public static void Enter_password_in_information_page(string value)
        {
            IWebElement pass = driver.FindElement(By.Name("password"));
            pass.SendKeys(value);
            Assert.AreEqual(pass.GetAttribute("value"), value);
        }
        public static void Enter_confirm_password_in_information_page(string value)
        {
            IWebElement confirmpass = driver.FindElement(By.Name("confirmPassword"));
            confirmpass.SendKeys(value);
            Assert.AreEqual(confirmpass.GetAttribute("value"), value);
        }

        [Obsolete]
        public static void Click_registration_button()
        {
            IWebElement regclick = driver.FindElement(By.Name("register"));
            regclick.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//b[text()=' Note: Your user name is Admin.']")));
        }

        public static void Assert_after_registration(string value)
        {
            IWebElement assertuser = driver.FindElement(By.XPath("//b[text()=' Note: Your user name is Admin.']"));
            Assert.AreEqual(assertuser.Text, "Note: Your user name is " + value); 
        }
    }
}
