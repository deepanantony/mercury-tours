﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace NUnitTestProject1
{
    public class Select_Flight:Setuppage
    {
        public static void Click_the_flight_button_for_booking()
        {
            IWebElement flights = driver.FindElement(By.CssSelector("a[href*='mercuryreservation.php']"));
            Thread.Sleep(2000);
            flights.Click();
        }

        public static void Select_oneway_trip()
        {
            IWebElement oneway = driver.FindElement(By.XPath("//input[@value='oneway' and @name='tripType']"));
            oneway.Click();
        }
        public static void Select_passenger_counts(string value)
        {
            IWebElement passenger = driver.FindElement(By.Name("passCount"));
            SelectElement select = new SelectElement(passenger);
            select.SelectByValue(value);
            var count = select.SelectedOption;
            Assert.AreEqual(count.GetAttribute("value"), value);
        }
        public static void Select_departing_from(string value)
        {
            IWebElement depart = driver.FindElement(By.Name("fromPort"));
            SelectElement select = new SelectElement(depart);
            select.SelectByValue(value);
            var place = select.SelectedOption;
            Assert.AreEqual(place.Text, "London");
        }
        public static void Select_departing_month(string value)
        {
            IWebElement month = driver.FindElement(By.Name("fromMonth"));
            SelectElement select = new SelectElement(month);
            select.SelectByValue(value);
            var months = select.SelectedOption; 
            Assert.AreEqual(months.Text, "May");
        }
        public static void Select_departing_date(string value)
        {
            IWebElement date = driver.FindElement(By.Name("fromDay"));
            SelectElement select = new SelectElement(date);
            select.SelectByValue(value);
            var day = select.SelectedOption;
            Assert.AreEqual(day.Text, "20");
        }
        public static void Select_arriving_in(string value)
        {
            IWebElement arrive = driver.FindElement(By.Name("toPort"));
            SelectElement select = new SelectElement(arrive);
            select.SelectByValue(value);
            var place = select.SelectedOption;
            Assert.AreEqual(place.Text, "Paris");
        }
        public static void Select_returning_month(string value)
        {
            IWebElement month = driver.FindElement(By.Name("toMonth"));
            SelectElement select = new SelectElement(month);
            select.SelectByValue(value);
            var months = select.SelectedOption;
            Assert.AreEqual(months.Text, "July");
        }
        public static void Select_returning_date(string value)
        {
            IWebElement date = driver.FindElement(By.Name("toDay"));
            SelectElement select = new SelectElement(date);
            select.SelectByValue(value);
            var days = select.SelectedOption;
            Assert.AreEqual(days.Text, "15");
        }
        public static void Select_service_class()
        {
            IWebElement oneway = driver.FindElement(By.XPath("//input[@value='First' and @name='servClass']"));
            oneway.Click();
        }
        public static void Select_airline(string value)
        {
            IWebElement lines = driver.FindElement(By.Name("airline"));
            SelectElement select = new SelectElement(lines);
            select.SelectByText(value);
            var air = select.SelectedOption;
            Assert.AreEqual(air.Text, "Unified Airlines");
        }
        public static void Click_continue_button()
        {
            IWebElement continues = driver.FindElement(By.Name("findFlights"));
            continues.Click();
        }
        public static void Assert_after_continue()
        {
            IWebElement assertflight = driver.FindElement(By.XPath("//font[text()='DEPART']"));
            Assert.AreEqual(assertflight.Text, "DEPART");
        }
        public static void Select_depart_flight()
        {
            IWebElement flight = driver.FindElement(By.XPath("//input[@name='outFlight' and @value='Unified Airlines$363$281$11:24']"));
            flight.Click();
        }
        public static void Select_return_flight()
        {
            IWebElement flight = driver.FindElement(By.XPath("//input[@name='inFlight' and @value='Pangea Airlines$632$282$16:37']"));
            flight.Click();
        }
        public static void Select_flight_continue_button()
        {
            IWebElement continues = driver.FindElement(By.Name("reserveFlights"));
            continues.Click();
        }
        public void Assert_after_select_flight()
        {
            IWebElement assertflight = driver.FindElement(By.XPath("//font[text()='DEPART']"));
            Assert.AreEqual(assertflight.Text, "DEPART");
        }
        
    }
}
