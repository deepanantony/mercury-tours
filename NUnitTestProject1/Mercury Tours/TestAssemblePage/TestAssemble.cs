﻿using System;
using System.Collections.Generic;
using System.Text;
using static NUnitTestProject1.Setuppage;
using static NUnitTestProject1.Register;
using static NUnitTestProject1.Select_Flight;
using static NUnitTestProject1.Flight_Booking;
using static NUnitTestProject1.Utilities;

namespace NUnitTestProject1
{
    public class TestAssemble
    {

        [Obsolete]
        public static void Complete_signin_page()
        {
            Launch_the_application("chrome");
            Enter_username_in_sign_in_page("Admin");
            Enter_password_in_sign_in_page("Test@123");
            Take_Screenshot();
            Click_sign_in_button();
        }

        [Obsolete]
        public static void Complete_register_page()
        {
            Click_register_for_new_user();
            Take_Screenshot();
            Enter_firstname_in_register_page("Deepan");
            Enter_lastname_in_register_page("Antony");
            Enter_phone_number_in_register_page("9994684117");
            Enter_email_id_in_register_page("deepan.antony@psiog.com");
            Enter_address_in_register_page("CBI Colony");
            Enter_city_in_register_page("Chennai");
            Enter_state_in_register_page("Tamil Nadu");
            Select_country_in_register_page("92");
            Enter_username_in_information_page("Admin");
            Enter_password_in_information_page("Test@123");
            Enter_confirm_password_in_information_page("Test@123");
            Take_Screenshot();
            Click_registration_button();
            Assert_after_registration("Admin.");
        }
        public static void Select_flight_page()
        {
            Click_the_flight_button_for_booking();
            Take_Screenshot();
            Select_oneway_trip();
            Select_passenger_counts("2");
            Select_departing_from("London");
            Select_departing_month("5");
            Select_departing_date("20");
            Select_arriving_in("Paris");
            Select_returning_month("7");
            Select_returning_date("15");
            Select_service_class();
            Select_airline("Unified Airlines");
            Take_Screenshot();
            Click_continue_button();
            Assert_after_continue();
            Select_depart_flight();
            Select_return_flight();
            Select_flight_continue_button();
        }
        public static void Complete_flight_booking_page()
        {
            Take_Screenshot();
            Enter_firstname_in_passengers_field("Deepan", 0);
            Enter_lastname_in_passengers_field("Antony", 0);
            Select_meal_options("VGML", 0, "Vegetarian");
            Enter_firstname_in_passengers_field("Ragu", 1);
            Enter_lastname_in_passengers_field("Ram", 1);
            Select_meal_options("HNML", 1, "Hindu");
            Enter_credit_card_number_in_credit_card_field("1234567");
            Select_expired_month(6);
            Select_expired_year("2010");
            Enter_credit_card_number_first_name("Mohan");
            Enter_credit_card_number_middle_name("Dass");
            Enter_credit_card_number_last_name("Pravin");
            Take_Screenshot();
            Click_secure_purchase();
            Click_logout_button();
            Assert_after_logout();
            Take_Screenshot();

        }
    }
}
