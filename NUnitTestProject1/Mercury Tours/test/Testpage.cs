using NUnit.Framework;
using System.Data;
using static NUnitTestProject1.Setuppage;
using static NUnitTestProject1.TestAssemble;

namespace NUnitTestProject1
{
    [TestFixture]
    public class Testpage
    {

        [SetUp]
        [System.Obsolete]
        public static void Open_and_login_the_application()
        {
            Complete_signin_page();
        }

        [Test]
        [System.Obsolete]
        public static void Registerpage()
        {
            Complete_register_page();
            Select_flight_page();
            Complete_flight_booking_page();
        }

        [TearDown]
        public static void Quitpage()
        {
            //Quit_the_application();
        }

    }


}